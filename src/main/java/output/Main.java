package output;

import models.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import org.eclipse.jgit.lib.Repository;

public class Main {
    public static void main(String[] args) {
        System.out.println("F-Droid Xml Reader");
        System.out.println("Informe a quantidade de aplicativos a baixar");
        Scanner sc = new Scanner(System.in);
        int qtdApps = sc.nextInt();
        System.out.println("Informe o caminho para clonar os repositorios");
        sc = new Scanner(System.in);
        String pathClone = sc.next();

        GitService gitService = new GitService();
        GitHistoryBadSmellMiner gitHistoryBadSmellMiner = new GitHistoryBadSmellMiner();

        try{
            FdroidReader fd = new FdroidReader();
            ArrayList<App> repositorios = fd.getRepositoryApps(qtdApps);

            for(App rep : repositorios){
                System.out.println(rep.getAppId());
                long qtdReviews = Reviews.countReviews(rep.getAppId());

                if(qtdReviews > 0) {
                    System.out.println("Qtd. Reviews: " + qtdReviews);

                    //Clona Repositorio
                    Repository r = gitService.cloneIfNotExists(pathClone + rep.getAppId(), rep.getRepository());

                    //Criar Diretório das análises
                    File file = new File(pathClone + rep.getAppId() + "/analises");
                    file.mkdir();

                    //Captura Reviews
                    file = new File(pathClone + rep.getAppId() + "/reviews");
                    file.mkdir();
                    Reviews.getreviewsApp(rep.getAppId(), pathClone,qtdReviews);

                    //Analisa repositorio com aDoctor
                    file = new File(pathClone + rep.getAppId() + "/analises/adoctor");
                    file.mkdir();
                    aDoctor.aDoctorAnalise(rep.getAppId(), pathClone);


                    //Analisa repositorio com PMD
                    file = new File(pathClone + rep.getAppId() + "/analises/pmd");
                    file.mkdir();
                    PmdCheck.detectAllSmells(r,"master",pathClone + rep.getAppId() + "/analises/pmd/");


                    //Analisa repositorio com Paprika





                }
                else{
                        System.out.println("Não foram encontradas Reviews para " + rep.getAppId());
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}

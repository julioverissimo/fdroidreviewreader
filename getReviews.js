//-----VAR
var args = process.argv.slice(2);
var fs = require('fs');
var gplay = require('google-play-scraper');
//var globalTunnel = require('global-tunnel-ng');
var totalPages = 0;
var path = args[1];
var paginas = args[2];

/*
function proxy(){
    globalTunnel.initialize({
        host: '10.0.0.10,
        port: 8080        
    });
}
*/

//--------FUNÇÃO PARA DELAY NA CHAMADA
function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

//FUNÇÃO RECURSIVA PARA CAPTURA DE REVIEWS
function reviews(pAppId, pPageNumber) {
	gplay.reviews({
	  appId: pAppId,
	  page: pPageNumber
	}).then(
	function(value){
		//console.log(value.length);		
		fs.writeFile(path + "/reviews" + "/" + pAppId + "-" + pPageNumber + ".json", JSON.stringify(value), function(err) {
			if(err) {
				return console.log(err);
			}		  
		});        
		
		sleep(5000);
		
		if(pPageNumber <= paginas){
			reviews(pAppId, pPageNumber+1);
			totalPages =  pPageNumber+1;
			//console.log("Pagina de Comentario " + totalPages);
		}
		/*else{
			console.log(totalPages);
		}*/
	}, console.log);
}

//INICIO
reviews(args[0], 0);


//globalTunnel.end();

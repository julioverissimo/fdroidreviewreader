package models;

public class ReviewFields {
    public String id;
    public String userName;
    public String userImage;
    public String date;
    public String url;
    public String score;
    public String title;
    public String text;
    public String replyDate;
    public String replyText;
}

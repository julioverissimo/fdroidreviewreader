package models;

import com.google.common.collect.Lists;
import net.sourceforge.pmd.PMD;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

import java.util.Date;
import java.util.List;

public class PmdCheck {
    public static void detectAllSmells(Repository repository, String branch, String pathResultado) throws Exception {
        try {
            Git git = new Git(repository);
            GitService gitService = new GitService();
            List<Ref> branches = git.branchList().call();
            String path = repository.getDirectory().toString().substring(0,repository.getDirectory().toString().length() - 4);

            for(Ref branche : branches) {
                String branchName = branche.getName();
                System.out.println("Commits of branch: " + branchName);
                System.out.println("-------------------------------------");

                Iterable<RevCommit> commits = git.log().add(repository.resolve(branchName)).call();
                List<RevCommit> commitsList = Lists.newArrayList(commits.iterator());

                for (RevCommit commit : commitsList) {
                    System.out.println("Analisando Commit " + commit.getName());
                    System.out.println(commit.getAuthorIdent().getName());
                    System.out.println(new Date(commit.getCommitTime() * 1000L));
                    System.out.println(commit.getFullMessage());
                    gitService.checkout(repository, commit.getName()); //faz chechout no repositorio

                    String[] arguments = {"-d",
                            path,
                            "-f",
                            "xml",
                            "-R",
                            "category/java/design.xml/ExcessiveMethodLength, category/java/design.xml/GodClass", //Long Method and God Class
                            "-r",
                            pathResultado + commit.getName() +".xml" };

                    PMD.run(arguments);
                }
            }



        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

package output;

import net.sourceforge.pmd.PMD;

import java.io.File;

public class AnalyzeApps {
    public static void main(String[] args) {
        try {

            String path = "C:\\Users\\julio\\Desktop\\AppsToAnalize.tar\\AppsToAnalize\\";
            String pastaResultadoAnalise = "C:\\Users\\julio\\OneDrive\\Documentos\\resultadoanalises\\";
            File file = new File(path);
            File afile[] = file.listFiles();
            int i = 0;
            for (int j = afile.length; i < j; i++) {
                path = path + afile[i].getName();
                String[] arguments = {"-d",
                        path,
                        "-f",
                        "xml",
                        "-R",
                        "category/java/design.xml/ExcessiveMethodLength, category/java/design.xml/GodClass", //Long Method and God Class
                        "-r",
                        pastaResultadoAnalise + afile[i].getName() + ".xml"};

                PMD.run(arguments);

            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
    }
}

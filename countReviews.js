//-----VAR
var args = process.argv.slice(2);
var fs = require('fs');
var gplay = require('google-play-scraper');
var totalPages = 0;
var path = args[1];

//--------FUNÇÃO PARA DELAY NA CHAMADA
function sleep(delay) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + delay);
}

//FUNÇÃO RECURSIVA PARA CAPTURA DE REVIEWS
function reviews(pAppId, pPageNumber) {
	gplay.reviews({
	  appId: pAppId,
	  page: pPageNumber
	}).then(
	function(value){
		
		sleep(5000);
		
		if(value.length == 40){
			reviews(pAppId, pPageNumber+1);
			totalPages =  pPageNumber+1;
		}
		else{
			console.log(totalPages * 40);
		}
	}, console.log);
}

//INICIO
reviews(args[0], 0);


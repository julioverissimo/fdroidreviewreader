package models;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Reviews {

    public static long countReviews(String appId){
        long qtdReviews = 0;
        BufferedReader br = null;
        try{
            Process processo = Runtime.getRuntime().exec("node details.js " + appId );
            br = new BufferedReader(new InputStreamReader(processo.getInputStream()));
            String retorno = br.readLine();

            if(retorno.contains("{ [Error: App not found (404)] status: 404 }")){
                return 0;
            }
            else if(retorno.contains("{ [Error: Error requesting Google Play:undefined] status: 503 }")){
                System.out.println("Error requesting Google Play:undefined] status: 503");
                return 0;
            }
            else{
                qtdReviews = Integer.parseInt(retorno);
            }

        }
        catch(Exception ex){
            return 0;
        }

        return qtdReviews;

    }

    public static void getreviewsApp(String appId,String pathReview, long qtdReviews){
        try{
            long qtdpaginas = Math.round(qtdReviews / 40);
            List<ReviewFields> lista = new ArrayList<ReviewFields>();
            List<ReviewFields> listaAux = null;

            Process processo = Runtime.getRuntime().exec("node getReviews.js " + appId + " " + pathReview + "/" + appId +"/ " + qtdpaginas );
            /*
            BufferedReader br = null;
            br = new BufferedReader(new InputStreamReader(processo.getInputStream()));
            String output;
            while((output = br.readLine()) != null){
                System.out.println(output);
            }
            */

            /*
            for(int i=0;i<=qtdpaginas;i++ ){
                BufferedReader j = new BufferedReader(new FileReader(pathReview  + appId + "/"+ appId +"-" + i +".json"));
                System.out.println(pathReview + "/" + appId + "/"+ appId +"-" + i +".json");
                Gson json = new Gson();
                Type collectionType = new TypeToken<List<ReviewFields>>() {}.getType();
                listaAux = json.fromJson(j, collectionType);

                lista.addAll(listaAux);
            }


            for(ReviewFields comentario : lista){
                System.out.println("-----------------------------------------------------------");
                System.out.println("Nota: " + comentario.score + " - " + comentario.date + " - " + comentario.text);
                System.out.println("-----------------------------------------------------------");
            }


            System.out.println("Total de Reviews: " + lista.size());
            */


            /*
            BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String linhaAnalise;

            FileWriter arqAnalise = new FileWriter(pathReview + "/" + appId + "/"+ appId +".csv");
            PrintWriter grArq = new PrintWriter(arqAnalise);

            while((linhaAnalise = b.readLine()) != null)
                grArq.printf(linhaAnalise);

            arqAnalise.close();
            */


        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

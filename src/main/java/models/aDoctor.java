package models;

import java.util.ArrayList;
import java.util.List;

public class aDoctor {
    public static void aDoctorAnalise(String appId,String pathReview){
        try{
            List<String> smells = new ArrayList<String>();
            smells.add("100000000000000");
            smells.add("010000000000000");
            smells.add("001000000000000");
            smells.add("000100000000000");
            smells.add("000010000000000");
            smells.add("000001000000000");
            smells.add("000000100000000");
            smells.add("000000010000000");
            smells.add("000000001000000");
            smells.add("000000000100000");
            smells.add("000000000010000");
            smells.add("000000000001000");
            smells.add("000000000000100");
            smells.add("000000000000010");
            smells.add("000000000000001");

            for(String smell : smells ){
                Process p = Runtime.getRuntime().exec("java -cp aDoctor-1.0-SNAPSHOT-jar-with-dependencies.jar it.unisa.aDoctor.process.RunAndroidSmellDetection "+ pathReview + "/" + appId + "/ " + pathReview + "/" + appId + "/analises/adoctor" +"/analise_" + smell +".csv " + smell);
            }


        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
